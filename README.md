# Electron Webpack Pixi Template

This is a template project for an electron game made using Pixi. Currently it supports the following features out of the box:

 - Typescript
 - Webpack config for Images, audio and gonts
 - Custom Icons
 - Dummy pixi application with keyboard + gamepad support
 - Configurable screen size with env vars
 - Builds for all major operating systems
 - Gitlab CI for windows and Linux builds
 - Code formatting with prettier
 - Unit testing with Jest
 - Linting with ESLint

## Getting Started

To use this as a basis for your own game do the following:

clone repo and remove .git directory

```
git clone https://gitlab.com/heggroup/electron-webpack-pixi-template.git
mv electron-webpack-pixi-template your-game-name
cd your-game-name
rm -rf .git
```

After this you will want to update a lot of the details in `package.json` to refelct your new project.

After that you should be ready to start [developing](#developing). 

## Demo Game

The "game" in this pack is a simple demo of keyboard and gamepad interactivity.
Move the logo with wasd/↑↓←→/analog sticks, change the color with gamepad buttons and exit the game with ESC/start

## Developing

Install the dependencies using

```
yarn install
```

and then start the service in dev mode with

```
yarn start
```

tests can be ran with

```
yarn test
```

the web app can be packaged with

```
yarn package
```

and bundled into an electron executable with

```
yarn make
```