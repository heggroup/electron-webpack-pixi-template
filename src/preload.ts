import { contextBridge, ipcRenderer } from "electron";
import { ElectronAPI } from "./window";

const electronAPI: ElectronAPI = {
  exit: () => ipcRenderer.send("exit"),
};

contextBridge.exposeInMainWorld("electronApi", electronAPI);
