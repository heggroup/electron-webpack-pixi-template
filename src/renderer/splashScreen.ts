import coolpng from "../assets/img/cool.png";
import coolmp3 from "../assets/audio/cool.mp3";

export const splashScreen = (): HTMLElement => {
  const div = document.createElement("div");
  div.className = "fade";
  div.id = "fade";

  const img = document.createElement("img");
  img.className = "img";
  img.id = "coolpng";
  img.src = coolpng;
  img.width = 300;
  img.height = 300;
  img.style.paddingTop = "20vh";
  img.style.paddingBottom = "5vh";

  const coolText = document.createElement("div");
  coolText.className = "div";
  coolText.id = "coolword";
  coolText.innerHTML = "HEG.COOL";

  div.appendChild(img);
  div.appendChild(coolText);

  setTimeout(() => new Audio(coolmp3).play(), 200);

  return div;
};
