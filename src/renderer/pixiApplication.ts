import { Application, Container, DisplayObject, Text } from "pixi.js";

export const pixiApplication = () => {
  const app = new Application<HTMLCanvasElement>({
    resizeTo: window,
    backgroundColor: 0x000000,
  });

  app.view.style.position = "absolute";
  app.view.style.display = "block";

  const stage: Container = new Container();

  const message = new Text("PIXI", {
    fontFamily: "VCR",
    fontSize: 32,
    fill: "white",
  });

  message.anchor.set(0.5, 0);
  message.position.set(app.view.width / 2, app.view.height / 2);
  stage.addChild(message as DisplayObject);

  app.renderer.render(stage);

  function gameLoop(): void {
    requestAnimationFrame(gameLoop);
    app.renderer.render(stage);
  }

  let up = false;
  let down = false;
  let left = false;
  let right = false;
  let controllerActive = false;

  window.addEventListener("gamepadconnected", (e) => {
    e.gamepad.vibrationActuator.playEffect("dual-rumble", {
      startDelay: 0,
      duration: 250,
      weakMagnitude: 1,
      strongMagnitude: 1,
    });
    setTimeout(() => {
      e.gamepad.vibrationActuator.playEffect("dual-rumble", {
        startDelay: 0,
        duration: 250,
        weakMagnitude: 1,
        strongMagnitude: 1,
      });
    }, 400);
  });

  window.onkeydown = (e) => {
    switch (e.key) {
      case "ArrowUp":
      case "w":
        up = true;
        break;
      case "ArrowDown":
      case "s":
        down = true;
        break;
      case "ArrowLeft":
      case "a":
        left = true;
        break;
      case "ArrowRight":
      case "d":
        right = true;
        break;
      case "Escape":
        window.electronApi.exit();
        break;
    }
  };

  window.onkeyup = (e) => {
    switch (e.key) {
      case "ArrowUp":
      case "w":
        up = false;
        break;
      case "ArrowDown":
      case "s":
        down = false;
        break;
      case "ArrowLeft":
      case "a":
        left = false;
        break;
      case "ArrowRight":
      case "d":
        right = false;
        break;
    }
  };

  app.ticker.add(() => {
    navigator.getGamepads().forEach((pad) => {
      if (pad) {
        if (pad.axes[0] === 0 && pad.axes[1] === 0) {
          if (controllerActive) {
            up = false;
            down = false;
            left = false;
            right = false;
          }
          controllerActive = false;
        } else {
          if (pad.axes[0] > 0.3) {
            left = false;
            right = true;
            controllerActive = true;
          } else if (pad.axes[0] < -0.3) {
            left = true;
            right = false;
            controllerActive = true;
          } else {
            left = false;
            right = false;
          }

          if (pad.axes[1] > 0.3) {
            up = false;
            down = true;
            controllerActive = true;
          } else if (pad.axes[1] < -0.3) {
            up = true;
            down = false;
            controllerActive = true;
          } else {
            up = false;
            down = false;
          }
        }

        if (pad.buttons[0].pressed) {
          message.style.fill = "green";
          return;
        }
        if (pad.buttons[1].pressed) {
          message.style.fill = "red";
          return;
        }
        if (pad.buttons[2].pressed) {
          message.style.fill = "blue";
          return;
        }
        if (pad.buttons[3].pressed) {
          message.style.fill = "yellow";
          return;
        }
        if (pad.buttons[9].pressed) {
          window.electronApi.exit();
        }
        message.style.fill = "white";
      }
    });
  });

  app.ticker.add((delta) => {
    let dx = 0;
    let dy = 0;
    if (up) {
      dy -= 10;
    }
    if (down) {
      dy += 10;
    }
    if (left) {
      dx -= 10;
    }
    if (right) {
      dx += 10;
    }

    if (dx != 0 && dy != 0) {
      dx = dx / Math.sqrt(2);
      dy = dy / Math.sqrt(2);
    }

    message.x += dx * delta * 0.2;
    message.y += dy * delta * 0.2;
  });

  gameLoop();

  return app.view;
};
