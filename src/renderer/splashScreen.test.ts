import { splashScreen } from "./splashScreen";

describe("Splash Screen", () => {
  test("creates node with two children", () => {
    const node = splashScreen();
    expect(node.childElementCount).toBe(2);
  });
  test("contains an image and some text", () => {
    const node = splashScreen();

    const img = node.childNodes[0] as HTMLImageElement;
    const text = node.childNodes[1] as HTMLElement;

    expect(img.className).toBe("img");
    expect(text.className).toBe("div");
  });
});
