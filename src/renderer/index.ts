import "../index.css";
import { pixiApplication } from "./pixiApplication";
import { splashScreen } from "./splashScreen";
import silence from "../assets/audio/silent.mp3";

const loadApp = () => {
  document.body.innerHTML = null;

  const splash = splashScreen();

  document.body.appendChild(splash);

  const startPixiApplication = async () => {
    document.body.removeChild(splash);
    document.body.appendChild(await pixiApplication());
  };

  setTimeout(startPixiApplication, 2500);
};

const test = new Audio(silence);
test
  .play()
  .then(() => loadApp())
  .catch(() =>
    document.body.appendChild(clickToStart(() => test.play().then(loadApp))),
  );

function clickToStart(loadApp: () => Promise<void>): HTMLElement {
  const div = document.createElement("div");
  div.style.height = "100vh";
  div.style.width = "100vw";
  div.style.display = "flex";
  div.style.justifyContent = "center";
  div.style.alignItems = "center";

  div.onclick = loadApp;

  const txt = document.createElement("p");
  txt.innerHTML = "Click to Start";
  txt.className = "click-to-start";

  div.appendChild(txt);

  return div;
}
