export type ElectronAPI = {
  exit: () => void;
};

declare global {
  interface Window {
    electronApi: ElectronAPI;
  }
}
