import { app, BrowserWindow, ipcMain, nativeImage } from "electron";
import coolIcon from "../assets/img/icons/cool-smol.png";

declare const MAIN_WINDOW_WEBPACK_ENTRY: string;
declare const MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY: string;

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require("electron-squirrel-startup")) {
  app.quit();
}

const registerListeners = () => {
  ipcMain.on("exit", () => {
    app.quit();
  });
};

const createWindow = (): void => {
  const mainWindow = new BrowserWindow({
    height: Number.parseInt(process.env.HEIGHT ?? "600"),
    width: Number.parseInt(process.env.WIDTH ?? "800"),
    resizable: false,
    backgroundColor: "#000000",
    webPreferences: {
      preload: MAIN_WINDOW_PRELOAD_WEBPACK_ENTRY,
    },
    icon: nativeImage.createFromPath(coolIcon),
  });

  mainWindow.setMenuBarVisibility(false);
  mainWindow.setMenu(null);
  mainWindow.removeMenu();

  mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);

  if (process.env.DEV_TOOLS) {
    mainWindow.webContents.openDevTools();
  }

  registerListeners();
};

app.on("ready", createWindow);

app.on("window-all-closed", app.quit);

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
